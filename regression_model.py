from abc import ABC, abstractmethod

from optimizers import Optimizer
from regularization import Regularization
from lr_policy import LearningRatePolicy

import tensorflow as tf
import numpy as np


class RegressionModel(ABC):

    def __init__(self, regularization=None, momentum=None, lr_policy=None):
        """
        """
        self.regularization = regularization
        self.optimizer = momentum
        self.lr_policy = lr_policy

    def get_full_cost(self, X, theta, y, N):
        loss = self.cost(X, theta, y) / tf.cast(N, tf.float64)
        if self.regularization:
            loss += self.regularization.regularization_cost(theta)

        return loss

    @abstractmethod
    def cost(self, X, theta, y):
        pass

    @abstractmethod
    def hypothesis(self, X, theta):
        pass

    @abstractmethod
    def batch_gradient_descent(self, X, y, theta, lr, batch_size, num_epochs=1, epoch_callback=None):
        pass


class NPRegressionModel(RegressionModel):

    def __init__(self, regularization=None, momentum=None, lr_policy=None):
        """

        :type lr_policy: LearningRatePolicy
        :type momentum: Optimizer
        :type regularization: Regularization
        """
        super().__init__(regularization, momentum, lr_policy)
        self.regularization = regularization
        self.optimizer = momentum
        self.lr_policy = lr_policy

    def get_full_cost(self, X, theta, y, N):
        loss = self.cost(X, theta, y) / N
        if self.regularization:
            loss += self.regularization.regularization_cost(theta)

        return loss

    @abstractmethod
    def gradient(self, X, theta, y):
        pass

    def gradient_descent_step(self, X, theta, y, lr):

        grad = self.gradient(X, theta, y)
        if self.regularization:
            grad += self.regularization.regularization_gradient(theta)

        if self.optimizer:
            grad = self.optimizer.get_smoothed_gradient(grad)

        return theta - lr * grad

    def batch_gradient_descent(self, X, y, theta, lr, batch_size, num_epochs=1, epoch_callback=None):
        """
        :param epoch_callback: (callback(epoch_n, theta, loss) -> None)
        :param X: (numpy array) rows: examples ,  cols: features
        :param y: (numpy array) target vector
        :param theta: (numpy array) model parameters
        :param lr: (float) learning rate
        :param batch_size: (int) training batch size per iteration
        :param num_epochs: (int) total number of passes through the data
        :return theta: (numpy array) optimized model parameters
        """

        num_examples = X.shape[0]
        steps_per_epoch = int(num_examples / batch_size) + 1
        for epoch_n in range(num_epochs):
            for step in range(steps_per_epoch):
                X_batch = X[step * batch_size: step * batch_size + batch_size]
                y_batch = y[step * batch_size: step * batch_size + batch_size]
                if len(X_batch) > 0:
                    theta = self.gradient_descent_step(X_batch, theta, y_batch, lr)
            loss = self.get_full_cost(X, theta, y, len(y))
            if epoch_callback:
                epoch_callback(epoch_n + 1, theta, loss, self.get_cost_function(theta))
            # print("Epoch {} loss: {}".format(epoch, loss))
            if self.lr_policy:
                lr = self.lr_policy.get_learning_rate(self, lr, epoch_n, theta, loss)

        return theta

    def get_model_function(self, theta):
        return lambda X: self.hypothesis(X, theta)

    def get_cost_function(self, theta):
        return lambda X, y: self.get_full_cost(X, theta, y, len(y))


class TFRegressionModel(RegressionModel):
    class Runparams:
        def __init__(self, X_var, y_var, writer):
            self.X_var = X_var
            self.y_var = y_var
            self.writer = writer

        @property
        def variables(self):
            return self.X_var, self.y_var, self.writer

    def __init__(self,
                 optimizer=tf.compat.v1.train.GradientDescentOptimizer,
                 optimizer_pararmeters={},
                 save_every_n_step=200,
                 regularization=None,
                 lr_policy=None,
                 saver_dir_path="./model/",
                 run_test_set_every_n_epochs=10):
        """

        :param optimizer: (tensorflow.optimizer.Optimizer.__init__)
        :param optimizer_pararmeters:
        :param regularization:
        :param lr_policy:
        :param output_dir_path:
        """

        super().__init__(regularization, optimizer, lr_policy)

        self.run_test_set_every_n_epochs = run_test_set_every_n_epochs
        self.saver_dir_path = saver_dir_path
        self.save_every_n_step = save_every_n_step
        self.optimizer_pararmeters = optimizer_pararmeters

    def define_model_placeholders_and_variables(self, theta):

        self.X_train = tf.placeholder("float", name="X_train")
        self.y_train = tf.placeholder("float", name="y_train")
        self.X_test = tf.placeholder("float", name="X_test")
        self.y_test = tf.placeholder("float", name="y_test")

        self.X = tf.Variable(np.empty((1, theta.shape[0])), dtype="float", name="X")
        self.y = tf.Variable(np.empty((1, 1)), dtype="float", name="y")
        self.W = tf.Variable(theta, name="W", dtype=tf.float32)

        self.epoch = tf.Variable(0, name="epochs", dtype=tf.float32)
        self.epoch_num = tf.Variable(self.run_test_set_every_n_epochs,
                                     name="run_test_set_every_n_epochs",
                                     dtype=tf.float32)

    def batch_gradient_descent(self, train_X, train_y, theta, lr, batch_size, num_epochs=1, epoch_callback=None,
                               test_X=None, test_y=None):

        with_test = test_y is not None

        # Initialize variables & datasets
        self.define_model_placeholders_and_variables(theta)

        train_dataset = tf.data.Dataset.from_tensor_slices((self.X, self.y)).batch(batch_size).repeat()
        iterator = tf.data.Iterator.from_structure(train_dataset.output_types,
                                                   train_dataset.output_shapes)
        train_data_init_op = iterator.make_initializer(train_dataset)

        if with_test:
            test_dataset = tf.data.Dataset.from_tensor_slices((self.X, self.y)).batch(test_y.shape[0])
            test_data_init_op = iterator.make_initializer(test_dataset)
        else:
            test_data_init_op = None

        self._init = tf.global_variables_initializer()

        if self.saver_dir_path:
            saver = tf.train.Saver()
        else:
            saver = None

        x, y = iterator.get_next()

        cost_op = self.get_full_cost(x, self.W, y, self.y.shape[0])
        cost_summary = tf.summary.scalar('loss', cost_op)

        # Start training
        with tf.Session() as sess:

            cost_func = lambda X, y: sess.run(cost_op, feed_dict={self.X: X, self.y: y})
            if self.lr_policy:
                lr = self.lr_policy.get_learning_rate_polcy(lr, cost_func)

            optimizer = self.optimizer(lr, **self.optimizer_pararmeters)
            train_op = optimizer.minimize(cost_op)

            # Run the initializer
            sess.run(self._init)

            train_writer = tf.summary.FileWriter('./logs/train_log')
            test_writer = tf.summary.FileWriter('./logs/test_log')

            train_vars = TFRegressionModel.Runparams(self.X_train, self.y_train, train_writer)
            test_vars =  TFRegressionModel.Runparams(self.X_test,  self.y_test,  test_writer )

            sess.run([train_data_init_op, test_data_init_op] if with_test else [train_data_init_op],
                     feed_dict={self.X_train: train_X,
                                self.y_train: train_y,
                                self.X_test: test_X,
                                self.y_test: test_y})

            steps_num = int(num_epochs * len(train_y) / batch_size) * batch_size
            steps_num += steps_num / self.run_test_set_every_n_epochs

            accuracy_op = tf.reduce_mean(tf.equal(tf.round(self.hypothesis(x, self.W)), y))
            accuracy_summary = tf.summary.scalar('accuracy', accuracy_op)

            # Fit all training data
            for step in range(steps_num):

                this_is_test = (step + 1) % self.run_test_set_every_n_epochs == 0
                if this_is_test:
                    cur_vars = test_vars
                else:
                    cur_vars = train_vars

                X_, y_, writer = cur_vars.variables

                self.X.assign(X_).eval()
                self.y.assign(y_).eval()

                # Perform step
                _, acc, cost = sess.run(train_op, accuracy_op, cost_op)
                writer.add_summary(cost_summary, cost)
                writer.add_summary(accuracy_summary, acc)

                # Save intermediate weights
                if saver and self.save_every_n_step is not None and self.save_every_n_step > 0 \
                        and ((step + 1) % self.save_every_n_step == 0):
                    saver.save(sess, self.saver_dir_path, global_step=step)

            return sess.run(self.W)
