from abc import ABC, abstractmethod
import numpy as np


class Optimizer(ABC):
    @abstractmethod
    def get_smoothed_gradient(self, current_gradient):
        pass

class ExponentialWeightedSumMomentum(Optimizer):
    def __init__(self, smoothing=0.3):
        self._old_grad = None
        self._k = smoothing

    def get_smoothed_gradient(self, current_gradient):
        if np.any(self._old_grad):
            self._old_grad = self._k * current_gradient + (1 - self._k) * self._old_grad
        else:
            self._old_grad = current_gradient
        return self._old_grad
