import abc
import functools

import numpy as np
from tensorflow.python.eager import context
from tensorflow.python.framework import ops
from tensorflow.python.keras.optimizer_v2.learning_rate_schedule import LearningRateSchedule
from tensorflow.python.ops import math_ops


class LearningRatePolicy(abc.ABC):

    def get_learning_rate_polcy(self, init_lr, get_loss_function):
        self.init_lr = init_lr
        self.get_loss = get_loss_function

    @abc.abstractmethod
    def get_learning_rate(self, lr):
        pass




class ReduceLROnPlateauPolicy(LearningRatePolicy):
    def __init__(self, cumulative_epochs_num=100, minimum_threshold_ratio=0.001, lr_factor=0.1):
        self.lr_factor = np.float64(lr_factor)
        self.minimum_threshold_ratio = minimum_threshold_ratio
        self.cumulative_epochs_num = cumulative_epochs_num
        self._i = 0
        self._sum_on_old = 0
        self._prev_loss = None

    def get_learning_rate(self, lr):

        loss = self.get_loss()
        if self._prev_loss is not None:
            self._sum_on_old += loss - self._prev_loss

        self._i += 1

        if not (self._i % self.cumulative_epochs_num):
            self._i = 0
            if self._sum_on_old / (lr * (self.cumulative_epochs_num - 1)) < self.minimum_threshold_ratio:
                print ("used policy")
                return lr * self.lr_factor
            self._sum_on_old = 0

        self._prev_loss = loss

        return lr

class ExponentialDecayLRPolicy(LearningRatePolicy):



    @staticmethod
    def exponential_decay(learning_rate,
                          global_step,
                          decay_steps,
                          decay_rate,
                          staircase=False,
                          name=None):
        """Applies exponential decay to the learning rate.

        When training a model, it is often recommended to lower the learning rate as
        the training progresses.  This function applies an exponential decay function
        to a provided initial learning rate.  It requires a `global_step` value to
        compute the decayed learning rate.  You can just pass a TensorFlow variable
        that you increment at each training step.

        The function returns the decayed learning rate.  It is computed as:

        ```python
        decayed_learning_rate = learning_rate *
                                decay_rate ^ (global_step / decay_steps)
        ```

        If the argument `staircase` is `True`, then `global_step / decay_steps` is an
        integer division and the decayed learning rate follows a staircase function.

        Example: decay every 100000 steps with a base of 0.96:

        ```python
        ...
        global_step = tf.Variable(0, trainable=False)
        starter_learning_rate = 0.1
        learning_rate = tf.compat.v1.train.exponential_decay(starter_learning_rate,
        global_step,
                                                   100000, 0.96, staircase=True)
        # Passing global_step to minimize() will increment it at each step.
        learning_step = (
            tf.compat.v1.train.GradientDescentOptimizer(learning_rate)
            .minimize(...my loss..., global_step=global_step)
        )
        ```

        Args:
          learning_rate: A scalar `float32` or `float64` `Tensor` or a Python number.
            The initial learning rate.
          global_step: A scalar `int32` or `int64` `Tensor` or a Python number. Global
            step to use for the decay computation.  Must not be negative.
          decay_steps: A scalar `int32` or `int64` `Tensor` or a Python number. Must
            be positive.  See the decay computation above.
          decay_rate: A scalar `float32` or `float64` `Tensor` or a Python number.
            The decay rate.
          staircase: Boolean.  If `True` decay the learning rate at discrete intervals
          name: String.  Optional name of the operation.  Defaults to
            'ExponentialDecay'.

        Returns:
          A scalar `Tensor` of the same type as `learning_rate`.  The decayed
          learning rate.

        Raises:
          ValueError: if `global_step` is not supplied.

        @compatibility(eager)
        When eager execution is enabled, this function returns a function which in
        turn returns the decayed learning rate Tensor. This can be useful for changing
        the learning rate value across different invocations of optimizer functions.
        @end_compatibility
        """
        decayed_lr = ExponentialDecayLRPolicy.ExponentialDecay(
            learning_rate, decay_steps, decay_rate, staircase=staircase, name=name)
        if not context.executing_eagerly():
            decayed_lr = decayed_lr(global_step)
        else:
            decayed_lr = functools.partial(decayed_lr, global_step)
        return decayed_lr


    class ExponentialDecay(LearningRateSchedule):
        """A LearningRateSchedule that uses an exponential decay schedule."""

        def __init__(
                self,
                initial_learning_rate,
                decay_steps,
                decay_rate,
                staircase=False,
                name=None):
            """Applies exponential decay to the learning rate.

            When training a model, it is often recommended to lower the learning rate as
            the training progresses. This schedule applies an exponential decay function
            to an optimizer step, given a provided initial learning rate.

            The schedule a 1-arg callable that produces a decayed learning
            rate when passed the current optimizer step. This can be useful for changing
            the learning rate value across different invocations of optimizer functions.
            It is computed as:

            ```python
            def decayed_learning_rate(step):
              return initial_learning_rate * decay_rate ^ (step / decay_steps)
            ```

            If the argument `staircase` is `True`, then `step / decay_steps` is
            an integer division and the decayed learning rate follows a
            staircase function.

            You can pass this schedule directly into a `tf.keras.optimizers.Optimizer`
            as the learning rate.
            Example: When fitting a Keras model, decay every 100000 steps with a base
            of 0.96:

            ```python
            initial_learning_rate = 0.1
            lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
                initial_learning_rate,
                decay_steps=100000,
                decay_rate=0.96,
                staircase=True)

            model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate=lr_schedule),
                          loss='sparse_categorical_crossentropy',
                          metrics=['accuracy'])

            model.fit(data, labels, epochs=5)
            ```

            The learning rate schedule is also serializable and deserializable using
            `tf.keras.optimizers.schedules.serialize` and
            `tf.keras.optimizers.schedules.deserialize`.

            Args:
              initial_learning_rate: A scalar `float32` or `float64` `Tensor` or a
                Python number.  The initial learning rate.
              decay_steps: A scalar `int32` or `int64` `Tensor` or a Python number.
                Must be positive.  See the decay computation above.
              decay_rate: A scalar `float32` or `float64` `Tensor` or a
                Python number.  The decay rate.
              staircase: Boolean.  If `True` decay the learning rate at discrete
                intervals
              name: String.  Optional name of the operation.  Defaults to
                'ExponentialDecay'.

            Returns:
              A 1-arg callable learning rate schedule that takes the current optimizer
              step and outputs the decayed learning rate, a scalar `Tensor` of the same
              type as `initial_learning_rate`.
            """
            super().__init__()
            self.initial_learning_rate = initial_learning_rate
            self.decay_steps = decay_steps
            self.decay_rate = decay_rate
            self.staircase = staircase
            self.name = name

        def __call__(self, step):
            with ops.name_scope_v2(self.name or "ExponentialDecay") as name:
                initial_learning_rate = ops.convert_to_tensor(
                    self.initial_learning_rate, name="initial_learning_rate")
                dtype = initial_learning_rate.dtype
                decay_steps = math_ops.cast(self.decay_steps, dtype)
                decay_rate = math_ops.cast(self.decay_rate, dtype)

                global_step_recomp = math_ops.cast(step, dtype)
                p = global_step_recomp / decay_steps
                if self.staircase:
                    p = math_ops.floor(p)
                return math_ops.multiply(
                    initial_learning_rate, math_ops.pow(decay_rate, p), name=name)

        def get_config(self):
            return {
                "initial_learning_rate": self.initial_learning_rate,
                "decay_steps": self.decay_steps,
                "decay_rate": self.decay_rate,
                "staircase": self.staircase,
                "name": self.name
            }