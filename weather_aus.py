import numpy as np
import pandas as pd
from scipy import stats
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
import matplotlib
import matplotlib.pyplot as plt

import lr_policy
from optimizers import ExponentialWeightedSumMomentum
from regularization import L2Regularization

matplotlib.use("TkAgg")

from logistic_regression import LogisticRegression, TFLogisticRegression
from result_analyzer import RegressionMetrics


def main():
    df = pd.read_csv('weatherAUS.csv')
    df = df.drop(columns=['Sunshine', 'Evaporation', 'Cloud3pm', 'Cloud9am', 'Location', 'RISK_MM', 'Date'], axis=1)

    # Let us get rid of all null values in df
    df = df.dropna(how='any')

    # It's time to remove the outliers in our data - we are using Z-score to detect and remove the outliers.
    z = np.abs(stats.zscore(df._get_numeric_data()))
    df = df[(z < 3).all(axis=1)]

    # Lets deal with the categorical cloumns now
    # simply change yes/no to 1/0 for RainToday and RainTomorrow
    df['RainToday'].replace({'No': 0, 'Yes': 1}, inplace=True)
    df['RainTomorrow'].replace({'No': 0, 'Yes': 1}, inplace=True)

    # See unique values and convert them to int using pd.getDummies()
    categorical_columns = ['WindGustDir', 'WindDir3pm', 'WindDir9am']
    # for col in categorical_columns:
    #     print(np.unique(df[col]))

    # Transform the categorical columns
    df = pd.get_dummies(df, columns=categorical_columns)
    # df.iloc[4:9]

    # Next step is to standardize our data - using MinMaxScaler
    scaler = preprocessing.MinMaxScaler()
    scaler.fit(df)
    df = pd.DataFrame(scaler.transform(df), index=df.index, columns=df.columns)
    # df.iloc[4:10]

    target = df['RainTomorrow']
    df = df.drop(['RainTomorrow'], axis=1)



    X_train, y_train = np.array(df), np.array([target]).T
    # train_data, test_data, train_target, test_target = train_test_split(train_data, train_target, test_size = 0.2)
    X_train, valid_data, y_train, valid_target = train_test_split(X_train, y_train, test_size=0.25)

    model = LogisticRegression()
    tf_model = TFLogisticRegression()

    init_theta = np.random.random((X_train.shape[1], 1))
    tf_model.batch_gradient_descent(X_train, y_train, init_theta, .1, 128, 1)

    exit()

    tf_result_params = run_logistic_regression_and_plot_results(tf_model, X_train, y_train, valid_data,
                                                                valid_target)
    result_params = run_logistic_regression_and_plot_results(model, X_train, y_train, valid_data, valid_target)

    params = RegressionMetrics.plot_and_get_confusion_matrix(valid_data, valid_target,
                                                             model.get_model_function(result_params), 0.8)
    tf_params = RegressionMetrics.plot_and_get_confusion_matrix(valid_data, valid_target,
                                                                model.get_model_function(tf_result_params), 0.8)

    pass
    # model = LogisticRegression(momentum=ExponentialWeightedSumMomentum())
    # result_params = run_logistic_regression_and_plot_results(model, train_data, train_target, valid_data, valid_target)
    #
    # model = LogisticRegression(regularization=L2Regularization())
    # result_params = run_logistic_regression_and_plot_results(model, train_data, train_target, valid_data, valid_target)

    # model = LogisticRegression(lr_policy=lr_policy.ReduceLROnPlateauPolicy())
    # result_params = run_logistic_regression_and_plot_results(model, train_data, train_target, valid_data, valid_target, 100)


def run_logistic_regression_and_plot_results(model, train_data, train_target, valid_data, valid_target, num_epochs=10):
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlim([0, num_epochs + 1])

    graph = []

    def callback(i, cost_train, cost_valid):
        graph.append([i, cost_train, cost_valid])
        graph2 = np.array(graph)
        plt.plot(graph2[:, 0], graph2[:, 1], graph2[:, 0], graph2[:, 2])
        plt.pause(0.001)

    return run_batch_GD(model, train_data, train_target, valid_data, valid_target, loss_function_callback=callback,
                        num_epochs=num_epochs)


def run_batch_GD(model, X_train, y_train, X_valid, y_valid, init_theta=None, lr=.1, batch_size=64, num_epochs=200,
                 loss_function_callback=None):
    if not init_theta:
        init_theta = np.random.random((X_train.shape[1], 1))

    def callback(epoch_n, theta, loss, cost_function):
        print("Cost after epoch #{} is {}".format(epoch_n, loss))
        if loss_function_callback:
            loss_function_callback(epoch_n, cost_function(X_train, y_train), cost_function(X_valid, y_valid))

    return model.batch_gradient_descent(X_train, y_train, init_theta, lr, batch_size, num_epochs, callback)


if __name__ == "__main__":
    main()
.1