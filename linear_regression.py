from __future__ import print_function

import pandas as pd
import tensorflow as tf
import numpy as np

from regression_model import TFRegressionModel, NPRegressionModel


class TFLinearRegression(TFRegressionModel):

    def hypothesis(self, X, W):
        return tf.matmul(X, W)

    def cost(self, X, W, y):
        loss = tf.reduce_sum(tf.pow(self.hypothesis(X, W) - y, 2))
        tf.print(loss, [loss])
        return loss


class LinearRegression(NPRegressionModel):

    def hypothesis(self, X, theta):
        return np.dot(X, theta)

    def cost(self, X, theta, y):
        y = y.reshape(-1, 1)
        return np.power(np.dot(X, theta) - y, 2)

    def gradient(self, X, theta, y):
        return 2 *np.dot(X.T, self.hypothesis(X,theta) - y.reshape(-1, 1)) / y.shape[0]



if __name__ == "__main__":
    # load the WW2 weather data
    df = pd.read_csv("weatherww2.csv")

    # shuffle the data
    df = df.sample(frac=1)

    # add bias column to data

    # prepare it for training (grab only relevant columns, split to X (samples) and y (target)
    X = df[["MinTemp"]]
    X = (X - X.mean()) / X.std()
    X.insert(0, 'bias', 1.)

    y = df[["MaxTemp"]]
    #
    # # normalize the data
    # X["MinTemp"] = ((X["MinTemp"] - X["MinTemp"].mean()) / X["MinTemp"].std())

    # initialize theta (model parameters)
    initial_theta = np.array([[0], [0]])

    # configure training hyperparameters
    lr = .05
    batch_size = 64
    num_epochs = 5

    X = np.array(X)
    y = np.array(y)

    def callback(epoch_n, theta, loss, cost_function):
        pass

    # start training and return optimal parameters
    model = TFLinearRegression()
    print(model.batch_gradient_descent(X, y, initial_theta, lr, batch_size, num_epochs, callback))

    model = LinearRegression()
    print(model.batch_gradient_descent(X, y, initial_theta, lr, batch_size, num_epochs))

    pass
