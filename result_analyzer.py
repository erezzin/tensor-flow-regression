import matplotlib.pyplot as plt
import heatmap
import numpy as np


class RegressionMetrics:

    @staticmethod
    def get_confusion_matrix_parameters(X_valid, y_valid, result_function, threshold):
        y_pred = RegressionMetrics.passes_threshold(result_function(X_valid), threshold).astype(np.int32)
        are_equal = np.equal(y_pred, y_valid)

        tp_tn_fp_fn = len(y_pred)
        tp_tn = np.count_nonzero(are_equal)
        tp_fp = np.count_nonzero(y_pred)
        tp_fn = np.count_nonzero(y_valid)

        fn = (tp_tn_fp_fn - tp_tn - tp_fp + tp_fn) / 2
        tp = tp_fn - fn
        fp = tp_fp - tp
        tn = tp_tn - tp

        return tp, fp, tn, fn

    @staticmethod
    def get_metrics(tp, fp, tn, fn):
        accuracy = (tn + tp) / float(tp + fp + tn + fn)
        precision = tn / float(tp + fp)
        recall = tp / float(tp + fn)

        return accuracy, precision, recall

    @staticmethod
    def plot_and_get_confusion_matrix(X_valid, y_valid, result_function, threshold):
        tp, fp, tn, fn = RegressionMetrics.get_confusion_matrix_parameters(X_valid, y_valid, result_function, threshold)

        fig, ax = plt.subplots()

        im, cbar = heatmap.heatmap(np.array([[tp, fn], [fp, tn]]), ["Result True", "Result False"], ["Predicted True", "Predicted False"], ax=ax, cmap="YlGnBu")
        heatmap.annotate_heatmap(im, valfmt="{x}")

        fig.tight_layout()
        plt.show()

        return tp, fp, tn, fn

    @staticmethod
    def passes_threshold(col, threshold):
        return (col > threshold) * 1
