import numpy as np
import pandas as pd
from sklearn.utils import shuffle
import tensorflow as tf

from regression_model import TFRegressionModel, NPRegressionModel

class TFLogisticRegression(TFRegressionModel):

    @staticmethod
    def sigmoid(H):
        """
        :param H: (numpy array) array of logits after applying model parameters on samples
        :return: (numpy array) array of activated logits
        """
        return 1.0 / (1 + tf.exp(-H))

    @staticmethod
    def logistic_function(X, theta):
        """
        :param X: (numpy array) Set of samples you apply the model on
        :param theta: (numpy array) Model parameters
        :return H: (numpy array or scalar) Model's output on the sample set
        """
        logits = tf.matmul(X, theta)
        return TFLogisticRegression.sigmoid(logits)

    def hypothesis(self, X, theta):
        return TFLogisticRegression.logistic_function(X, theta)

    def cost(self, X, theta, y):
        """
        :param X: (numpy array) Set of samples you apply the model on
        :param theta: (numpy array) Model parameters
        :param y: (numpy array) Target vector (ground truth for each sample)
        :return cost: (scalar) The model's parameters mean loss for all samples
        """
        y = tf.reshape(y, (-1, 1))
        H = self.logistic_function(X, theta)
        loss = -tf.reduce_sum((y * tf.log(H) + (1 - y) * tf.log(1 - H)), 1)
        tf.Print(loss, [loss])
        return loss


class LogisticRegression(NPRegressionModel):

    @staticmethod
    def sigmoid(H):
        """
        :param H: (numpy array) array of logits after applying model parameters on samples
        :return: (numpy array) array of activated logits
        """
        return 1.0 / (1 + np.exp(-H))

    @staticmethod
    def logistic_function(X, theta):
        """
        :param X: (numpy array) Set of samples you apply the model on
        :param theta: (numpy array) Model parameters
        :return H: (numpy array or scalar) Model's output on the sample set
        """
        logits = np.dot(X, theta)
        return LogisticRegression.sigmoid(logits)

    def hypothesis(self, X, theta):
        return LogisticRegression.logistic_function(X, theta)

    def cost(self, X, theta, y):
        """
        :param X: (numpy array) Set of samples you apply the model on
        :param theta: (numpy array) Model parameters
        :param y: (numpy array) Target vector (ground truth for each sample)
        :return cost: (scalar) The model's parameters mean loss for all samples
        """
        y = y.reshape(-1, 1)
        H = self.logistic_function(X, theta)
        return -np.sum((y * np.log(H) + (1 - y) * np.log(1 - H)))

    def gradient(self, X, theta, y):
        """
        :param X: (numpy array) Set of samples you apply the model on
        :param theta: (numpy array) Model parameters
        :param y: (numpy array) Target vector (ground truth for each sample)
        :return gradient: (numpy array or scalar) parameter gradients for current step
        """
        H = self.logistic_function(X, theta)
        return np.dot(X.T, H - y.reshape(-1, 1)) / y.shape[0]


def main():
    # load data and shuffle
    df = pd.read_csv('data_banknote_authentication.csv')
    df = shuffle(df)
    X = df.iloc[:, :-1]
    X = ((X - X.mean()) / X.std())

    X.insert(0, 'bias', 1.0)
    y = df.iloc[:, -1]

    # initialize theta (model parameters)
    num_features = X.shape[1]
    theta = np.random.rand(num_features, 1)

    # configure training hyperparameters
    LR = 0.01
    BATCH_SIZE = 64
    NUM_EPOCHS = 100

    X = np.array(X)
    y = np.array(y)

    # start training
    model = TFLogisticRegression()
    print(model.batch_gradient_descent(X, y, theta, LR, BATCH_SIZE, NUM_EPOCHS))

    model = LogisticRegression()
    print(model.batch_gradient_descent(X, y, theta, LR, BATCH_SIZE, NUM_EPOCHS))


if __name__ == '__main__':
    main()
