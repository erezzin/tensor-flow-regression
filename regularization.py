from abc import ABC

import numpy as np


class Regularization(ABC):

    def regularization_cost(self, theta):
        pass

    def regularization_gradient(self, theta):
        pass


class L2Regularization(Regularization):
    def __init__(self, param = 0.3):
        self.param = param

    def regularization_cost(self, theta):
        return self.param * np.inner(theta.T, theta.T)

    def regularization_gradient(self, theta):
        return 2 * self.param * theta
